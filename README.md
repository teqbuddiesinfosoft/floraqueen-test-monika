## Laravel Test

### Prerequisite

-   Php 8.2
-   Composer version 2
-   Stop local mysql or apache to make 3306 and 80 port available

### Setup

1.

```bash
git clone https://gitlab.com/teqbuddiesinfosoft/floraqueen-test-monika.git
```

2.

```bash
cd floraqueen-test-monika
```

3.

```bash
cp .env.example .env
```

4.

```bash
composer install
```

5.

```bash
php artisan sail:install
```

6.

```bash
./vendor/bin/sail build
```

7.

```bash
./vendor/bin/sail up -d
```

8. After docker up, Goto terminal of docker image and run below command.
```bash
php artisan migrate --seed
```

9.
```bash
./vendor/bin/sail npm run dev
```

10. Visit [http://localhost:81](http://localhost:81)

## Test 1 [Link](http://localhost:81/order)
![Alt text](Practical-1.png)

## Test 2 [Link](http://localhost:81/) [We have no create product API. So, integration is pending for it.]
![Alt text](Practical-2.png)

## Test 3
```bash
How would you build an asynchronous and distributed architecture,
how would you ensure that no information was lost and in case
there were failures, detect them?
```
Answer :-
I am not much aware about this to be honest but have some idea that I am going to explain below

1) We can implement some reliable message queue system like RabbitMQ which enable asynchronous communication between components.

2) We can do data replication so if one node fails, another can take over without losing any data.

3) We can implement error handling mechanisms for message processing.

4) We can do regular backups for critical components.

5) We can do monitoring and alerting for components. So we can identify if any data loss during failures.
